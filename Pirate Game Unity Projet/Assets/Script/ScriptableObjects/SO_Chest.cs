﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[CreateAssetMenu(fileName = "New Chest Type", menuName = "Object/Chest Type")]
public class SO_Chest : ScriptableObject
{
    [Label("Name Of Chest")]
    [Tooltip("The name of the object (String)")]
    public string S_Name;

    [Label("Money Value")]
    [Tooltip("How many money the player win (Int)")]
    public int I_MoneyValue;
    [Label("Rarity Of Object")]
    [Tooltip("The rarity of the object , determine the value and the drop chance (Int)")]
    public int I_Rarity;

    [Label("Weight Of Object")]
    [Tooltip("Change the deplacement of the player when he carry the object (Float)")]
    public float F_Weight = 0;

    [ShowAssetPreview]
    [Label("Picture Of Chest (Sprite)")]
    public Sprite Sp_Image;

}
