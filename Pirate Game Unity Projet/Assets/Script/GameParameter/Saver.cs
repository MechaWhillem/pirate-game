﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary; //encodage binairs de valeur
using System.IO; //gestion des flux de streams

[System.Serializable]
public class Data
{
    public int I_MoneySave; // Player Money
}

public class Saver : MonoBehaviour
{
    public void load()
    {
        if (File.Exists(Application.persistentDataPath + "/save.dat"))
        {
            BinaryFormatter decoder = new BinaryFormatter();
            FileStream stream = File.OpenRead(Application.persistentDataPath + "/save.dat");
            Data data = (Data)decoder.Deserialize(stream);

            GameManager.instance.I_Money = data.I_MoneySave;

            stream.Close();
            Debug.Log("Load Finish");
        }
    }

    public void Save()
    {
        Data data = new Data();

        data.I_MoneySave = GameManager.instance.I_Money;

        BinaryFormatter encoder = new BinaryFormatter();
        FileStream stream = File.OpenWrite(Application.persistentDataPath + "/save.dat");
        encoder.Serialize(stream, data);
        stream.Close();
        Debug.Log("Save Finish");
    }
}
