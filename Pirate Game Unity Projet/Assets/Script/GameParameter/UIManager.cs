﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;

public class UIManager : MonoBehaviour
{
    [Header("UI Player")]
    [Label("Money Display")]
    [Tooltip("Display the current Money of the Player (TMPU)")]
    public TextMeshProUGUI TMPU_MoneyDisplay;

    private void Update()
    {
        TMPU_MoneyDisplay.text = "Money : "+GameManager.instance.I_Money;
    }

    public void ButtonCanInteract()
    {
        GameManager.instance.B_CanInteract = true;
    }
}
