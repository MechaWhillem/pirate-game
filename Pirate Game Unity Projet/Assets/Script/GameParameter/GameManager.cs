﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class GameManager : MonoBehaviour
{
    [Header("Value")]
    [Label("Money")]
    [Tooltip("How many Money have the player (Int)")]
    public int I_Money;

    [Header("Boolean")]
    [Tooltip("If the player can Interact/Move (Boolean)")]
    [Label("Can Interact")]
    public bool B_CanInteract = true;

    public static GameManager instance;

    [Button]
    public void Load()
    {
        GetComponent<Saver>().load();
    }

    [Button]
    public void Save()
    {
        GetComponent<Saver>().Save();
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
