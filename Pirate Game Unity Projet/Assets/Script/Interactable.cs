﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public GameObject GO_DiplayButton;
    public bool B_Grab = false;
    public bool B_Interact = false;

    private void Update()
    {
        if (B_Grab == true)
        {
            transform.position = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position;
            B_Interact = false;
        } 

        if (B_Interact == false)
        {
            GO_DiplayButton.SetActive(false);
        } else
        {
            GO_DiplayButton.SetActive(true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (B_Grab == false)
            {
                B_Interact = true;
            }

            if (Input.GetButtonUp("Interaction") && B_Grab == false)
            {
                B_Grab = true;
            } else if (Input.GetButtonUp("Interaction") && B_Grab == true)
            {
                B_Grab = false;
            }
        }
        else 
        {
            B_Interact = false;
        }
    }


}
