﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ZoneDepot : MonoBehaviour
{
    public List<GameObject> LG_ListOfObjects = new List<GameObject>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("DropableLoot"))
        {
            LG_ListOfObjects.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        LG_ListOfObjects.Remove(collision.gameObject);
    }
}
