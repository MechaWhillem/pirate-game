﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class PNJ : MonoBehaviour
{
    [Label("UI Display")]
    [Tooltip("The UI diplayed when the Player interact with thel (GameObject)")]
    public GameObject G_UI;

    [Header("Jobs")]

    [Label("The Collector of Gold")]
    [Tooltip("The Collector of Gold are the PNJ who bought loot from the Player (Bool)")]
    [ShowIf(ConditionOperator.And, "ShipperInvert", "SmithInvert")]
    public bool B_AreGold = false;
    [Label("The Shipper")]
    [Tooltip("The Shipper are the PNJ for the Ship of the Player (Bool)")]
    [ShowIf(ConditionOperator.And, "GoldInvert", "SmithInvert")]
    public bool B_AreShipper = false;
    [Label("The Black Smith")]
    [Tooltip("The Black Smith are the PNJ who makes weapons for the Player (Bool)")]
    [ShowIf(ConditionOperator.And, "GoldInvert", "ShipperInvert")]
    public bool B_AreSmith = false;

    public bool GoldInvert() { return !B_AreGold; }
    public bool ShipperInvert() { return !B_AreShipper; }
    public bool SmithInvert() { return !B_AreSmith; }

    [Header("Attribute of Jobs")]

    [Label("Zone of Depot")]
    [Tooltip("The Zone where the Player drop these loot (GameObject)")]
    [ShowIf(ConditionOperator.And, "B_AreGold")]
    public GameObject G_ZoneOfDepot;

    private void Update()
    {
        if (B_AreGold == true)
        {
            //G_ZoneOfDepot.GetComponent<ZoneDepot>().LG_ListOfObjects
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (Input.GetButton("Interaction"))
            {
                DisplayUI();
            }
        }
    }

    public void DisplayUI()
    {
        G_UI.SetActive(true);
        GameManager.instance.B_CanInteract = false;
    }
}
