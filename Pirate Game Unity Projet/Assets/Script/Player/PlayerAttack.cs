﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class PlayerAttack : MonoBehaviour
{
    [Header("Attack")]
    public int I_Damage;
    public Transform T_AttackPos;
    public LayerMask LM_WhatIsEnemies;  
    public float F_StartTimeBtwAttack;
    [SerializeField]
    private float F_TimeBtwAttack;

    [Header("Range")]
    public float F_AttackRangeX;
    public float F_AttackRangeY;

    void Update()
    {

        if (F_TimeBtwAttack <= 0)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                Collider2D[] enemiesToDamage = Physics2D.OverlapBoxAll(T_AttackPos.position, new Vector2(F_AttackRangeX, F_AttackRangeY), 0, LM_WhatIsEnemies);
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    //enemiesToDamage[i].GetComponent<EnemieBase>().TakeDamage(I_Damage);
                }
                F_TimeBtwAttack = F_StartTimeBtwAttack;
            }

        }
        else
        {
            F_TimeBtwAttack -= Time.deltaTime;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(T_AttackPos.position, new Vector2(F_AttackRangeX, F_AttackRangeY));
    }
}